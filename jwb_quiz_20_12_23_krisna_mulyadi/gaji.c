#include "gaji.h"
#include <stdio.h>
#include <string.h>

void jdl_aplikasi()
{
    printf("Selamat datang di Aplikasi Perhitungan Gaji\n");
    // Informasi Menu
}

void gapok_tunja(char gol, char *status, float *gapok, float *tunja)
{
    // implementasi penghitungan gaji pokok dan tunjangan berdasarkan gol dan status

    *gapok = 5000;  // Replace with actual calculation
    *tunja = 2000;  // Replace with actual calculation
}

float prosen_potongan(float gapok)
{
    // hitung persen potongan berdasarkan gaji pokok
    return 0.1;  // 10% potongan, replace with actual calculation
}

float potongan(float gapok, float tunja, float prosen_pot)
{
    // hitung potongan berdasarkan gapok, tunja, proses potongan
    return (gapok + tunja) * prosen_pot;
}

float gaji_bersih(float gapok, float tunja, float pot)
{
    // hitung gaji_bersih based on gapok, tunja, dan potongan
    return gapok + tunja - pot;
}

void input(char *nama, char *gol, char *status)
{
    //  input  nama, gol, and status

    printf("Masukkan Nama: ");
    scanf("%s", nama);
    printf("Masukkan Golongan (A/B/C): ");
    scanf(" %c", gol);
    printf("Masukkan Status (Menikah/Belum): ");
    scanf("%s", status);
}

void output(float gapok, float tunja, float pot, float gaber)
{
    // output  gaji pokok, tunjangan, potongan, gaji bersih
    printf("Gaji Pokok   : %.2f\n", gapok);
    printf("Tunjangan    : %.2f\n", tunja);
    printf("Potongan     : %.2f\n", pot);
    printf("Gaji Bersih  : %.2f\n", gaber);
}

