#ifndef GAJI_H
#define GAJI_H

void jdl_aplikasi();

void gapok_tunja(char gol, char *status, float *gapok, float *tunja);

float prosen_potongan(float gapok);

float potongan(float gapok, float tunja, float prosen_pot);

float gaji_bersih(float gapok, float tunja, float pot);

void input(char *nama, char *gol, char *status);

void output(float gapok, float tunja, float pot, float gaber);

#endif // GAJI_H

