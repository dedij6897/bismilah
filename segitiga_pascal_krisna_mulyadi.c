#include <stdio.h>
int factorial(int n) {
    if (n <= 1)
        return 1;
    else
        return n * factorial(n - 1);
}
int combination(int n, int r) {
    return factorial(n) / (factorial(r) * factorial(n - r));
}

int main() {
    int n;
    printf("Masukkan n: ");
    scanf("%d", &n);

    for (int i = 0; i < n; i++) {
        for (int j = 0; j <= n - i - 2; j++) {
            printf(" ");
        }
        for (int j = 0; j <= i; j++) {
            printf("%d ", combination(i, j));
        }
        printf("\n");
    }

    return 0;
}




/*hasil

Masukkan n: 5
    1
   1 1
  1 2 1
 1 3 3 1
1 4 6 4 1

--------------------------------
Process exited after 1.753 seconds with return value 0
Press any key to continue . . .*/
