#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdbool.h>

int main()
{
 
int i, j; // Deklarasi

    printf("Inputkan nilai i: "); //input
    scanf("%i", &i);

    printf("Inputkan nilai j: ");
    scanf("%i", &j);

    printf("---------------------------\n");
    printf("| operasi | hasil operasi |\n");
    printf("---------------------------\n");
    printf("| %d + %d   | %7d       |\n",i,j,(i+j));
    printf("| %d - %d   | %7d       |\n",i,j,(i-j));
    printf("| %d * %d   | %7d       |\n",i,j,(i*j));
    printf("| %d / %d   | %10.2f    |\n",i,j,((float) i/j));
    printf("| %d div %d | %7d       |\n",i,j,(i/j));
    printf("| %d mod %d | %7d       |\n",i,j,(i%j));
    printf("---------------------------\n\n");
    
    return 0;
}

    


