#include <stdio.h>
int main() {
  float initial, step, final;
  printf("Masukan suhu awal: ");
  scanf("%f", &initial);
  printf("Masukan step: ");
  scanf("%f", &step);
  printf("Masukan suhu akhir: ");
  scanf("%f", &final);
  printf("---------------------------------\n");
  printf("| Celcius | Fahrenheit | Kelvin |\n");
  printf("---------------------------------\n");
  for (float celsius = initial; celsius <= final; celsius += step) {
    float fahrenheit = (celsius * 9 / 5) + 32;
    float kelvin = celsius + 273.15;
    printf("|   %.2f   |    %.2f    |  %.2f  |\n", celsius, fahrenheit, kelvin);
  }
  printf("---------------------------------\n");
  return 0;
}
